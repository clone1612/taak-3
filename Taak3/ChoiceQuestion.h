/*
 * ChoiceQuestion.h
 *
 *  Created on: 1 dec. 2013
 *      Author: Jannick
 */

#ifndef CHOICEQUESTION_H_
#define CHOICEQUESTION_H_

#include "Question.h"
#include <vector>

class choiceQuestion: public Question {
public:
	choiceQuestion(const unsigned int& vectorSize);

	void parseQuestion(const string& vraag, vector<string> choices, const int& amountChoices);
    void setChoices(const vector<string> newChoices);
    void printChoices() const;

	const int& amountChoices();
    const string& choiceAt(const int& idx) const;
    void setAmountChoices(const unsigned int& newAmountChoices);

private:
    int amountChoices_;
	vector<string> choices_;
};

#endif /* CHOICEQUESTION_H_ */
