/*
 * Question.cpp
 *
 *  Created on: 25 nov. 2013
 *      Author: Jannick
 */

#include "Question.h"

Question::Question(const unsigned int& vectorSize):
vectorSize(vectorSize), typeIndex_(0), textIndex_(1), answerIndex_(2), answered(0), next_(NULL), prev_(NULL) {
    thequestion_.resize(vectorSize);
}

// Print de vraag
void Question::show_question() const {
	cout << thequestion_.at(typeIndex_) << " " << thequestion_.at(textIndex_) << endl;
}

// Geeft het vraagtype van de vraag terug
const string& Question::questionType() const {
    return thequestion_.at(typeIndex_);
}

// Geeft de vraagtekst van de vraag terug
const string& Question::questionText() const {
    return thequestion_.at(textIndex_);
}

// Geeft het antwoord van de vraag terug
const string& Question::questionAnswer() const {
    return thequestion_.at(answerIndex_);
}

// Past het vraagtype van de vraag aan
void Question::setQuestionType(const string& newQuestionType) {
    thequestion_[typeIndex_] = newQuestionType;
}

// Past de vraagtekst van de vraag aan
void Question::setQuestionText(const string& newQuestionText) {
    thequestion_[textIndex_] = newQuestionText;
}

void Question::setQuestionAnswer(const string& answer) {
    thequestion_[answerIndex_] = answer;
    answered = 1;
}

void Question::setAnswerend(const int &newStatus) {
    answered = newStatus;
}

bool Question::is_answered() {
	if (answered == 1) {
		return true;
	}
	return false;
}
