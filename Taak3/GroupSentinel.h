/*
 * GroupSentinel.h
 *
 *  Created on: 6 dec. 2013
 *      Author: Jannick
 */

#ifndef GROUPSENTINEL_H_
#define GROUPSENTINEL_H_

#include "Question.h"

class groupSentinel: public Question {
public:
	groupSentinel(const int& groupID);
	const int& groupID() const;
private:
	int groupID_;
};

#endif /* GROUPSENTINEL_H_ */
