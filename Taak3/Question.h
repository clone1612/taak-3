/*
 * Question.h
 *
 *  Created on: 25 nov. 2013
 *      Author: Jannick
 */

#include <iostream>
#include <vector>
using namespace std;

#ifndef QUESTION_H_
#define QUESTION_H_

class Question {
public:
	Question(const unsigned int& vectorSize);

	virtual void show_question() const; // Print de volledige vraag

	const string& questionType() const; // Geeft het type v/d vraag terug
	const string& questionText() const; // Geeft de vraagtekst v/d vraag terug
    const string& questionAnswer() const;

    void setQuestionType(const string& newQuestionType); // Past het vraagtype v/e vraag aan
	void setQuestionText(const string& newQuestionText); // Past de vraagtekst v/e vraag aan
	void setQuestionAnswer(const string& answer); // Beantwoorden v/d vraag
    void setAnswerend(const int& newStatus);
	bool is_answered();

	int vectorSize, typeIndex_, textIndex_, answerIndex_, answered;
    vector<string> thequestion_;
    Question* next_;
    Question* prev_;
};

#endif /* QUESTION_H_ */
