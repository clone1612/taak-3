/*
 * GroupQuestion.h
 *
 *  Created on: 1 dec. 2013
 *      Author: Jannick
 */

#ifndef GROUPQUESTION_H_
#define GROUPQUESTION_H_

#include "Question.h"
#include "GroupSentinel.h"

class groupQuestion: public Question {
public:
	groupQuestion(const unsigned int& vectorSize, const unsigned int& groupID);

	void parseQuestion(const string& theme);
	const int& groupID() const;
    const int& amountSubgroups() const;
    void setAmountSubgroups(const int& newAmount);
    groupSentinel* sentinel();
    void setSentinel(groupSentinel * newSentinel);
private:
	int groupID_, amountSubgroups_;
    groupSentinel * sentinel_;
};

#endif /* GROUPQUESTION_H_ */
