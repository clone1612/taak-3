/*
 * Enquete.cpp
 *
 *  Created on: 25 nov. 2013
 *      Author: Jannick
 */

#include "Enquete.h"
#include "TextQuestion.h"
#include "BoolQuestion.h"
#include "ScaleQuestion.h"
#include "ChoiceQuestion.h"
#include "GroupQuestion.h"
#include "questionPath.h"
#include "GroupSentinel.h"
#include <fstream>
#include <stack>
#include <vector>
#include <algorithm>
using namespace std;

bool Enquete::checkCaseInsensitive(const string& a, const string& b) {
    unsigned int sz = a.size();
    if (b.size() != sz)
        return false;
    for (unsigned int i = 0; i < sz; ++i)
        if (tolower(a[i]) != tolower(b[i]))
            return false;
    return true;
}

bool Enquete::checkPath(const questionPath &path){
    questionPath curPath(1);
    for (Enquete::Iterator it = Enquete::begin(); it != Enquete::end(); it++) {
        if (path == curPath) { // Dit is het pad dat we zochten
            return true;
        }
        // Nog niet de juiste, loop verder door de enquete
        if (checkCaseInsensitive((*it)->questionType(), "GROUP")) {
            curPath.hadGroup();
            continue;
        }
        if (checkCaseInsensitive((*it)->questionType(), "SENTINEL")) {
            curPath.hadSentinel();
            continue;
        }
        else {
            curPath.hadOther();
            continue;
        }
    }
    return false;
}

bool Enquete::checkGroupPath(const questionPath &path) {
    questionPath curPath(1);
    for (Enquete::Iterator it = Enquete::begin(); it != Enquete::end(); it++) {
        if (path == curPath) { // Dit is het pad dat we zochten
            if (it->questionType() == "GROUP") {
                return true;
            }
        }
        // Nog niet de juiste, loop verder door de enquete
        if (checkCaseInsensitive((*it)->questionType(), "GROUP")) {
            curPath.hadGroup();
            continue;
        }
        if (checkCaseInsensitive((*it)->questionType(), "SENTINEL")) {
            curPath.hadSentinel();
            continue;
        }
        else {
            curPath.hadOther();
            continue;
        }
    }
    return false;
}

bool Enquete::checkQuestionType(const string& questionType) {
    if (checkCaseInsensitive(questionType, "text") || checkCaseInsensitive(questionType, "bool") || checkCaseInsensitive(questionType, "scale") || checkCaseInsensitive(questionType, "choice") || checkCaseInsensitive(questionType, "group")) {
        return true;
    }
    return false;
}

const int Enquete::amountQuestions() {
    int count = 0;
    for (Enquete::Iterator it = Enquete::begin(); it != Enquete::end(); it++) {
        if (it->questionType() == "GROUP") {
            continue;
        }
        else if (it->questionType() == "SENTINEL") {
            continue;
        }
        else {
            count++;
            continue;
        }
    }
    return count;
}

Enquete::Iterator Enquete::insert_after(Question *qBefore, Question *qNew) {
    qNew->prev_ = qBefore;
    qNew->next_ = qBefore->next_;
    if (qBefore->next_ == NULL) {
        last_ = qNew;
    }
    else {
        qBefore->next_->prev_ = qNew;
    }
    qBefore->next_ = qNew;
    Enquete::Iterator it(qNew);
    return it;
}

Enquete::Iterator Enquete::insert_before(Question *qAfter, Question *qNew) {
    qNew->prev_ = qAfter->prev_;
    qNew->next_ = qAfter;
    qAfter->prev_->next_ = qNew;
    qAfter->prev_ = qNew;
    Enquete::Iterator it(qNew);
    return it;
}

void Enquete::push_back(Question *qNew) {
    insert_after(last_, qNew);
}

void Enquete::pop_back() {
    last_ = last_->prev_;
    delete last_;
}

Enquete::Iterator Enquete::erase(Question *q) {
    Iterator it = Iterator(q->next_);
    q->prev_->next_ = q->next_;
    if (q->next_ == NULL) {
        last_ = q->prev_;
    }
    else {
        q->next_->prev_ = q->prev_;
    }
    delete q;
    return it;
}

bool Enquete::checkGroupAnswered(Iterator it) {
    groupQuestion * gq = dynamic_cast<groupQuestion *>(*it);
    int groupID = gq->groupID(), ok;
    while (it != Enquete::end()) { // We gaan op zoek naar de sentinel
        groupSentinel * gs = dynamic_cast<groupSentinel *>(*it);
        groupQuestion * gq2 = dynamic_cast<groupQuestion *>(*it);
        if (gs != NULL) { // Het is een sentinel
            if (gs->groupID() == groupID) { // Het is de gezochte sentinel
                gq->setAnswerend(1);
                break;
            }
            else { // Niet de gezcohte sentinel -> volgende loop
                it++;
                continue;
            }
        }
        if (gq2 != NULL) {
            if (checkGroupAnswered(it)) {
                it++;
                ok = 1;
                continue;
            }
            else {
                ok = 0;
                break;
            }
        }
        else { // Het is een gewone vraag
            if (it->is_answered()) {
                it++;
                ok = 1;
                continue;
            }
            else {
                ok = 0;
                break;
            }
        }
    }
    if (ok == 1) {
        return true;
    }
    else {
        return false;
    }
}

void Enquete::list() {
    questionPath curPath(1);
    for (Enquete::Iterator it = Enquete::begin(); it != Enquete::end(); it++) {
        groupQuestion * gq = dynamic_cast<groupQuestion *>(*it);
        groupSentinel * gs = dynamic_cast<groupSentinel *>(*it);
        if (gq != NULL) {
            curPath.printPath();
            cout << " " << gq->questionType() << " " << gq->amountSubgroups() << " " << gq->questionText() << endl;
            curPath.hadGroup();
            continue;
        }
        if (gs != NULL) {
            curPath.hadSentinel();
            continue;
        }
        else {
            curPath.printPath();
            cout << " ";
            it->show_question();
            curPath.hadOther();
            continue;
        }
    }
}

void Enquete::listAnswered() {
    questionPath curPath(1);
    for (Enquete::Iterator it = Enquete::begin(); it != Enquete::end(); it++) {
        groupQuestion * gq = dynamic_cast<groupQuestion *>(*it);
        groupSentinel * gs = dynamic_cast<groupSentinel *>(*it);
        if (gq != NULL) {
            curPath.printPath();
            if (checkGroupAnswered(it)) {
                cout << "OK" << endl;
            }
            else {
                cout << "NOK" << endl;
            }
            curPath.hadGroup();
            continue;
        }
        if (gs != NULL) {
            curPath.hadSentinel();
            continue;
        }
        else {
            curPath.printPath();
            cout << " ";
            if (it->is_answered()) {
                cout << "OK" << endl;
            }
            else {
                cout << "NOK" << endl;
            }
            curPath.hadOther();
            continue;
        }
    }
}

void Enquete::updatePath(stack<groupQuestion *> &groupStack, Question *curQuestion, questionPath &curPath) {
    groupQuestion * gq = dynamic_cast<groupQuestion *>(curQuestion);
    groupSentinel * gs = dynamic_cast<groupSentinel *>(curQuestion);
    if (gq != NULL) { // Het is een group question
        curPath.hadGroup();
        groupStack.push(gq);
    }
    else if (gs != NULL) { // Het is een group sentinel
        curPath.hadSentinel();
        groupStack.pop();
    }
    else {
        curPath.hadOther();
    }
}

void Enquete::updatePath(Question *curQuestion, questionPath &curPath) {
    groupQuestion * gq = dynamic_cast<groupQuestion *>(curQuestion);
    groupSentinel * gs = dynamic_cast<groupSentinel *>(curQuestion);
    if (gq != NULL) {
        curPath.hadGroup();
    }
    if (gs != NULL) {
        curPath.hadSentinel();
    }
    else {
        curPath.hadOther();
    }
}

void Enquete::add(const string& soort, const string& vraag, const unsigned int& scale_l, const unsigned int& scale_h) {
	vector<string> tempChoices;
	int n_choices = 0, done = 0;
    string input;
    if (checkCaseInsensitive(soort, "choice")) { // We moeten een choice toevoegen
        tempChoices.resize(1);
		while (done == 0) { // Loop om mogelijke coices op te vragen
			getline(cin, input);
			if (input == ".") {
				done = 1;
			}
			else {
				tempChoices.push_back(input);
				n_choices++;
			}
		}
		if (n_choices > 1) { // Zijn er wel genoeg (> 1) keuzes opgegeven?
            push_back(new choiceQuestion(5)); // Voeg toe aan einde v/d enquete
            choiceQuestion * cq = dynamic_cast<choiceQuestion *>(last_); // Omvormen naar subklasse
			cq->parseQuestion(vraag, tempChoices, n_choices); // Vraag vullen
            cq->setAmountChoices(n_choices);
			setChanged(1);
		}
		else { // Niet genoeg keuzes opgegeven
			cerr << "Niet genoeg geldige antwoorden." << endl;
			done = 1;
		}
	}
	if (checkCaseInsensitive(soort, "text")) { // We moeten een text-vraag toevoegen
		push_back(new textQuestion(5)); // Voeg toe aan einde v/d enquete
		textQuestion * tq = dynamic_cast<textQuestion *>(last_); // Omvormen naar subklasse
		tq->parseQuestion(vraag); // Vraag vullen
		setChanged(1);
	}
	if (checkCaseInsensitive(soort, "bool")) { // We moeten een bool-vraag toevoegen
		push_back(new boolQuestion(5)); // Voeg toe aan einde v/d enquete
		boolQuestion * bq = dynamic_cast<boolQuestion *>(last_); // Omvormen naar subklasse
		bq->parseQuestion(vraag); // Vraag vullen
		setChanged(1);
	}
	if (checkCaseInsensitive(soort, "scale")) { // We moeten een scale-vraag toevoegen
		push_back(new scaleQuestion(5)); // Voeg toe aan einde v/d enquete
		scaleQuestion * sq = dynamic_cast<scaleQuestion *>(last_); // Omvormen naar subklasse
		sq->parseQuestion(vraag, scale_l, scale_h); // Vraag vullen
		setChanged(1);
	}
}

void Enquete::insert(const string &soort, const string &vraag, const questionPath &path, const int& scale_l, const int& scale_h, const int& beforeSentinel) {
    int done = 0, n_choices = 0;
    stack<groupQuestion*> groupStack;
    vector<string> tempChoices;
	string input;
    questionPath curPath(1);
    for (Enquete::Iterator it = Enquete::begin(); it != Enquete::end() && done == 0; it++) {
        if (curPath == path) {
            if (checkCaseInsensitive(soort, "choice")) { // We moeten een choice toevoegen
                while (done == 0) { // Loop om mogelijke coices op te vragen
                    getline(cin, input);
                    if (input == ".") {
                        done = 1;
                    }
                    else {
                        tempChoices.push_back(input);
                        n_choices++;
                    }
                }
                if (n_choices > 1) { // Zijn er wel genoeg (> 1) keuzes opgegeven?
                    if (beforeSentinel == 1) {
                        groupQuestion * gq = dynamic_cast<groupQuestion *>(*it);
                        gq->setAmountSubgroups(gq->amountSubgroups() + 1);
                        it = insert_before(gq->sentinel(), new choiceQuestion(5));
                        choiceQuestion * cq = dynamic_cast<choiceQuestion *>(*it); // Omvormen naar subklasse
                        cq->parseQuestion(vraag, tempChoices, n_choices); // Vraag vullen
                        cq->setAmountChoices(n_choices);
                        setChanged(1);
                        break;
                    }
                    else {
                        it = insert_before(*it, new choiceQuestion(5)); // Voeg toe aan einde v/d enquete
                        choiceQuestion * cq = dynamic_cast<choiceQuestion *>(*it); // Omvormen naar subklasse
                        cq->parseQuestion(vraag, tempChoices, n_choices); // Vraag vullen
                        cq->setAmountChoices(n_choices);
                        setChanged(1);
                        break;
                    }
                }
                else { // Niet genoeg keuzes opgegeven
                    cerr << "Niet genoeg geldige antwoorden." << endl;
                    done = 1;
                }
            }
            if (checkCaseInsensitive(soort, "text")) { // We moeten een text-vraag toevoegen
                if (beforeSentinel == 1) {
                    groupQuestion * gq = dynamic_cast<groupQuestion *>(*it);
                    gq->setAmountSubgroups(gq->amountSubgroups() + 1);
                    it = insert_before(gq->sentinel(), new textQuestion(5)); // Voeg toe aan einde v/d enquete
                    textQuestion * tq = dynamic_cast<textQuestion *>(*it); // Omvormen naar subklasse
                    tq->parseQuestion(vraag); // Vraag vullen
                    setChanged(1);
                    break;
                }
                else {
                    it = insert_before(*it, new textQuestion(5)); // Voeg toe aan einde v/d enquete
                    textQuestion * tq = dynamic_cast<textQuestion *>(*it); // Omvormen naar subklasse
                    tq->parseQuestion(vraag); // Vraag vullen
                    setChanged(1);
                    break;
                }
            }
            if (checkCaseInsensitive(soort, "bool")) { // We moeten een bool-vraag toevoegen
                if (beforeSentinel == 1) {
                    groupQuestion * gq = dynamic_cast<groupQuestion *>(*it);
                    gq->setAmountSubgroups(gq->amountSubgroups() + 1);
                    it = insert_before(gq->sentinel(), new boolQuestion(5)); // Voeg toe aan einde v/d enquete
                    boolQuestion * bq = dynamic_cast<boolQuestion *>(*it); // Omvormen naar subklasse
                    bq->parseQuestion(vraag); // Vraag vullen
                    setChanged(1);
                    break;
                }
                else {
                    it = insert_before(*it, new boolQuestion(5)); // Voeg toe aan einde v/d enquete
                    boolQuestion * bq = dynamic_cast<boolQuestion *>(*it); // Omvormen naar subklasse
                    bq->parseQuestion(vraag); // Vraag vullen
                    setChanged(1);
                    break;
                }
            }
            if (checkCaseInsensitive(soort, "scale")) { // We moeten een scale-vraag toevoegen
                if (beforeSentinel == 1) {
                    groupQuestion * gq = dynamic_cast<groupQuestion *>(*it);
                    gq->setAmountSubgroups(gq->amountSubgroups() + 1);
                    it = insert_before(gq->sentinel(), new scaleQuestion(5)); // Voeg toe aan einde v/d enquete
                    scaleQuestion * sq = dynamic_cast<scaleQuestion *>(*it); // Omvormen naar subklasse
                    sq->parseQuestion(vraag, scale_l, scale_h); // Vraag vullen
                    setChanged(1);
                    break;
                }
                else {
                    it = insert_before(*it, new scaleQuestion(5)); // Voeg toe aan einde v/d enquete
                    scaleQuestion * sq = dynamic_cast<scaleQuestion *>(*it); // Omvormen naar subklasse
                    sq->parseQuestion(vraag, scale_l, scale_h); // Vraag vullen
                    setChanged(1);
                    break;
                }
            }
        }
        updatePath(groupStack, *it, curPath);
    }
    if (!groupStack.empty()) {
        groupQuestion * gq = groupStack.top();
        int oldAmountSubgroups = gq->amountSubgroups();
        gq->setAmountSubgroups(oldAmountSubgroups + 1);
    }
}

void Enquete::add(const string& vraag, vector<string> choices, const int& n_choices) {
    push_back(new choiceQuestion(5)); // Voeg toe aan einde v/d enquete
    choiceQuestion * cq = dynamic_cast<choiceQuestion *>(last_); // Omvormen naar subklasse
    cq->parseQuestion(vraag, choices, n_choices);
}

// Groep toevoegen op het einde v/d lijst
void Enquete::add_group(const string& thema, const int& amountSubgroups) {
    push_back(new groupQuestion(5, groupID_));
    groupID_++;
    groupQuestion * gq = dynamic_cast<groupQuestion *>(last_);
    gq->parseQuestion(thema);
    gq->setAmountSubgroups(amountSubgroups);
}

void Enquete::prepareGroupVector(vector<int>& groupVector) {
    int idx = 1;
    for (vector<int>::iterator it = groupVector.begin(); it != groupVector.end(); ++it) {
        if (groupVector[idx] != 0) {
            int count = 1, idx2 = idx + 1, max = groupVector[idx];
            while (count < max) {
                if (groupVector[idx2] != 0) {
                    groupVector[idx] = groupVector[idx] + groupVector[idx2];
                    count = count - groupVector[idx2];
                    count++;
                    idx2++;
                    continue;
                }
                else {
                    idx2++;
                    count++;
                }
            }
            idx++;
            continue;
        }
        else {
            idx++;
        }
    }
    idx = 1;
    int count = 1;
    for (vector<int>::iterator it = groupVector.begin(); it != groupVector.end(); ++it) {
        if (groupVector[idx] != 0) {
            groupVector[idx] = groupVector[idx] + count;
            idx++;
            count++;
            continue;
        }
        else {
            idx++;
            count++;
        }
    }
    //sort(groupVector.begin(), groupVector.end());
    groupVector.erase(std::remove(groupVector.begin(), groupVector.end(), 0), groupVector.end());
}

void Enquete::useGroupVector(vector<int> &groupVector) {
    int count = 1, idx = 0, groupIDvar = groupVector[0];
    groupVector.erase(groupVector.begin());
    vector<int> groupVectorCopy(groupVector);
    sort(groupVectorCopy.begin(), groupVectorCopy.end());
    for (Enquete::Iterator it = Enquete::begin(); it != Enquete::end(); it++) {
        if (count == groupVectorCopy[idx]) {
            it = insert_after(*it, new groupSentinel(groupID_ - groupIDvar));
            groupIDvar--;
            idx++;
            if (groupIDvar == 0) {
                break;
            }
            else {
                count++;
                continue;
            }
        }
        else {
            count++;
            continue;
        }
    }
    for (unsigned i=0; i<groupVector.size(); ++i) {
        groupVector[i]++;
    }
    count = 1;
    idx = 0;
    for (Enquete::Iterator it = Enquete::begin(); it != Enquete::end(); it++) {
        groupQuestion * group;
        if (it->questionType() == "GROUP") {
            groupQuestion * gq = dynamic_cast<groupQuestion *>(*it);
            group = gq;
            int count2 = 1;
            for (Enquete::Iterator it2 = Enquete::begin(); it2 != Enquete::end(); it2++) {
                if (count2 == groupVector[idx]) {
                    groupSentinel * gs = dynamic_cast<groupSentinel *>(*it2);
                    group->setSentinel(gs);
                    group = NULL;
                    idx++;
                    break;
                }
                else if (it2->questionType() == "SENTINEL") {
                    continue;
                }
                else {
                    count2++;
                    continue;
                }
            }
            continue;
        }
        else {
            count++;
        }
    }
}

void Enquete::add_group(const string& name, questionPath& path1, questionPath& path2, const int& amountSubgroups) {
    int added = 0;
    stack<groupQuestion*> groupStack;
    questionPath curPath(1);
    groupQuestion * group;
    for (Enquete::Iterator it = Enquete::begin(); it != Enquete::end(); it++) {
        if (path1 == curPath && added == 0) { // Dit is het begin van onze group en we zijn hier nog nooit geweest
            groupID_++;
            it = insert_before(*it, new groupQuestion(5, groupID_));
            groupQuestion * gq = dynamic_cast<groupQuestion *>(*it);
            gq->parseQuestion(name);
            gq->setAmountSubgroups(amountSubgroups);
            group = gq;
            added = 1;
            continue;
        }
        if (path2 == curPath) { // Dit is het einde van onze group
            groupQuestion * gq = dynamic_cast<groupQuestion *>(*it);
            if (gq != NULL) { // Het is ook een group
                it = insert_after(gq->sentinel(), new groupSentinel(groupID_));
                groupSentinel * gs = dynamic_cast<groupSentinel *>(*it);
                group->setSentinel(gs);
                break;
            }
            else {
                it = insert_after(*it, new groupSentinel(groupID_));
                groupSentinel * gs = dynamic_cast<groupSentinel *>(*it);
                group->setSentinel(gs);
                setChanged(1);
                break;
            }
        }
        updatePath(groupStack, *it, curPath);
    }
    if (!groupStack.empty()) {
        groupQuestion * gq = groupStack.top();
        int oldAmountSubgroups = gq->amountSubgroups();
        gq->setAmountSubgroups(oldAmountSubgroups - (amountSubgroups - 1));
    }
}

void Enquete::ungroup(questionPath& path) {
    int amountSubgroups;
    stack<groupQuestion*> groupStack;
    questionPath curPath(1);
    for (Enquete::Iterator it = Enquete::begin(); it != Enquete::end(); it++) {
        if (path == curPath) { // Dit is het pad dat we zochten
            groupQuestion * gq = dynamic_cast<groupQuestion *>(*it); // Omvormen
            amountSubgroups = gq->amountSubgroups();
            erase(gq->sentinel()); // Verwijder de sentinel v/d group
            erase(*it); // Verwijder de group-question zelf
            break;
        }
        updatePath(groupStack, *it, curPath);
    }
    if (!groupStack.empty()) {
        groupQuestion * gq = groupStack.top();
        int oldAmountSubgroups = gq->amountSubgroups();
        gq->setAmountSubgroups(oldAmountSubgroups + (amountSubgroups - 1));
    }
}

void Enquete::edit(const questionPath& path) {
    questionPath curPath(1);
    for (Enquete::Iterator it = Enquete::begin(); it != Enquete::end(); ++it) {
        if (path == curPath) { // Dit is het pad dat we zochten
            string oldQuestionText = (*it)->questionText(), newQuestionText;
            cout << "Nieuwe vraagtekst voor vraag ";
            curPath.printPath();
            cout << " (" << oldQuestionText << ")" << endl;
            getline(cin, newQuestionText);
            if (newQuestionText.empty()) {
                cerr << "Niets geantwoord. Vraagtekst is niet aangepast." << endl;
                break;
            }
            else {
                (*it)->setQuestionText(newQuestionText);
                cout << "Vraagtekst voor vraag ";
                curPath.printPath();
                cout << " is aangepast." << endl;
                setChanged(1);
                break;
            }
        }
        updatePath(*it, curPath);
    }
}

void Enquete::setChoices(const questionPath& path) {
    questionPath curPath(1);
    for (Enquete::Iterator it = Enquete::begin(); it != Enquete::end(); it++) {
        if (path == curPath) { // Dit is het pad dat we zochten
            choiceQuestion * cq = dynamic_cast<choiceQuestion *>(*it); // Omvormen
            string questionText = (*it)->questionText();
            cout << "Nieuwe antwoorden voor vraag ";
            curPath.printPath();
            cout << " (" << questionText << ") " << endl;
            int done = 0, choiceCount = 0;
            vector<string> tempChoices;
            tempChoices.resize(1);
            string input;
            while (done == 0) {
                getline(cin, input);
                if (input.empty()) {
                    cerr << "Een keuze mag niet leeg zijn." << endl;
                    continue;
                }
                if (input == ".") {
                    break;
                }
                else {
                    tempChoices.push_back(input);
                    choiceCount++;
                    continue;
                }
            }
            if (choiceCount > 1) {
                cq->setAmountChoices(choiceCount);
                cq->setChoices(tempChoices);
                cout << "Antwoorden voor vraag ";
                curPath.printPath();
                cout << " aangepast." << endl;
                break;
            }
            else {
                cerr << "Niet genoeg geldige antwoorden (< 2)." << endl;
                break;
            }
        }
        updatePath(*it, curPath);
    }
}

void Enquete::remove(const questionPath& path) {
    questionPath curPath(1);
    stack<groupQuestion*> groupStack;
    for (Enquete::Iterator it = Enquete::begin(); it != Enquete::end(); ++it) {
        if (path == curPath) { // Dit is het pad dat we zochten
            Enquete::erase(*it);
            setChanged(1);
            break;
        }
        updatePath(groupStack, *it, curPath);
    }
    if (!groupStack.empty()) {
        groupQuestion * gq = groupStack.top();
        int oldAmountSubgroups = gq->amountSubgroups();
        gq->setAmountSubgroups(oldAmountSubgroups - 1);
    }
}

void Enquete::setChanged(const int& changedStatus) {
    if (changedStatus == 0) {
        changed_ = 0;
    }
    else if (changedStatus == 1) {
        changed_ = 1;
    }
}

bool Enquete::notSaved() const {
    return changed_ == 1;
}

void Enquete::setID(const string& newID) {
    id_ = newID;
}

void Enquete::setVersion(const unsigned int& newVersion) {
    version_ = newVersion;
}

void Enquete::saveAnswers(const string &filename) {
    ofstream output(filename);
    if (output.is_open()) {
        questionPath curPath(1);
        output << "ID " << id_ << endl;
        for (Enquete::Iterator it = Enquete::begin(); it != Enquete::end(); ++it) {
            if (it->questionType() == "GROUP") {
                curPath.hadGroup();
                continue;
            }
            else if (it->questionType() == "SENTINEL") {
                curPath.hadSentinel();
                continue;
            }
            else {
                curPath.printPath(output, 1);
                output << " " << it->questionAnswer() << endl;
                curPath.hadOther();
                continue;
            }
        }
    }
    output.close();
}

void Enquete::save(const string& filename) {
	ofstream o_file(filename);
	if (o_file.is_open()) {
        cout << "File is open" << endl;
		int c_count = 1, steps = amountQuestions();
        questionPath curPath;
        curPath.parseString("1");
        Enquete::Iterator it = Enquete::begin();
		o_file << "VERSION " << version_ << endl;
		o_file << "ID " << id_ << endl;
		o_file << "STEPS " << steps << endl;
		while (it != Enquete::end()) {
            string questionType = it->questionType(), questionText = it->questionText();
			if (it->questionType() == "CHOICE") {
                choiceQuestion * cq = dynamic_cast<choiceQuestion *>(*it);
				curPath.printPath(o_file);
                o_file << " " << questionType << " " << cq->amountChoices() << " " << questionText << endl;
				while (c_count <= cq->amountChoices()) {
					o_file << cq->choiceAt(c_count) << endl;
					c_count++;
				}
                c_count = 1;
                curPath.hadOther();
                it++;
                continue;
			}
			if (it->questionType() == "TEXT" || it->questionType() == "BOOL") {
                curPath.printPath(o_file);
				o_file << " " << questionType << " " << questionText << endl;
                curPath.hadOther();
                it++;
                continue;
			}
            if (it->questionType() == "SCALE") {
                curPath.printPath(o_file);
                scaleQuestion * sq = dynamic_cast<scaleQuestion *>(*it);
                o_file << " " << questionType << " " << sq->scale_l() << " " << sq->scale_h() << " " << questionText << endl;
                curPath.hadOther();
                it++;
                continue;
            }
            if (it->questionType() == "GROUP") {
                curPath.printPath(o_file);
                groupQuestion * gq = dynamic_cast<groupQuestion *>(*it);
                o_file << " " << questionType << " " << gq->amountSubgroups() << " " << questionText << endl;
                curPath.hadGroup();
                it++;
                continue;
            }
            if (it->questionType() == "SENTINEL") {
                curPath.hadSentinel();
                it++;
                continue;
            }
		}
	}
	o_file.close();
	cout << "Bestand bewaard." << endl;
	setChanged(0);
}