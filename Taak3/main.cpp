/*
 * main.cpp
 *
 *  Created on: 28 nov. 2013
 *      Author: Jannick
 */

#include "Enquete.h"
#include "questionPath.h"
#include "ScaleQuestion.h"
#include "ChoiceQuestion.h"
#include "GroupQuestion.h"
#include "GroupSentinel.h"
#include <vector>
#include <sstream>
#include <fstream>
#include <uuid/uuid.h>
using namespace std;

string get_uuid() {
    uuid_t t;
    char ch[36];
    uuid_generate(t);
    uuid_unparse(t, ch);
    stringstream ss;
    string uuid;
    ss << ch;
    ss >> uuid;
    return uuid;
}

int asker(Enquete &enquete, const int &testMode = 0, const string &answerFile = "TEST") {
    questionPath curPath(1);
    int amountQuestions = enquete.amountQuestions(), idx = 1;
    cout << enquete.amountQuestions();
    Enquete::Iterator it = enquete.begin();
    while (it != enquete.end()) {
        if (it->questionType() == "GROUP") {
            ++it;
            ++idx;
            curPath.hadGroup();
            continue;
        }
        if (it->questionType() == "SENTINEL") {
            ++it;
            ++idx;
            curPath.hadSentinel();
            continue;
        }
        if (it->questionType() == "TEXT" || it->questionType() == "BOOL") {
            cout << "Vraag ";
            curPath.printPath(1);
            cout << " " << it->questionText() << endl;
        }
        if (it->questionType() == "SCALE") {
            scaleQuestion * sq = dynamic_cast<scaleQuestion *>(*it);
            cout << "Vraag ";
            curPath.printPath(1);
            cout << " " << sq->questionText() << endl;
            cout << "Antwoord moet liggen tussen: " << "(" << sq->scale_l() << "-" << sq->scale_h() << ")" << endl;
        }
        if (it->questionType() == "CHOICE") {
            choiceQuestion * cq = dynamic_cast<choiceQuestion *>(*it);
            cout << "Vraag ";
            curPath.printPath(1);
            cout << " " << cq->questionText() << endl;
            cout << "Maak een keuze uit: " << "(1-" << cq->amountChoices() << ")" << endl;
            cq->printChoices();
        }
        // Input verwerken
        string input, command;
        getline(cin, input);
        istringstream ss(input);
        ss >> command;
        if (command == ":quit") {
            if (testMode == 1) {
                break;
            }
            else {
                enquete.saveAnswers(answerFile);
                break;
            }
        }
        else if (command == ":next") {
            int amount = 0, count = 0;
            if (ss.good()) {
                ss >> amount;
                count = 1;
            }
            if ((idx + amount) > amountQuestions) {
                cout << "Next gaat niet, je zou buiten de enquete terechtkomen!" << endl;
                continue;
            }
            idx = idx + amount;
            while (count <= amount) {
                groupQuestion * gq = dynamic_cast<groupQuestion *>(*it);
                groupSentinel * gs = dynamic_cast<groupSentinel *>(*it);
                if (gq != NULL) {
                    curPath.hadGroup();
                }
                if (gs != NULL) {
                    curPath.hadSentinel();
                }
                if (gs == NULL && gq == NULL) {
                    curPath.hadOther();
                    count++;
                }
                ++it;
                continue;
            }
            while (it->questionType() == "GROUP" || it->questionType() == "SENTINEL" || it->answered == 1) {
                if (it->questionType() == "GROUP") {
                    curPath.hadGroup();
                    ++it;
                    ++idx;
                    continue;
                }
                else if (it->questionType() == "SENTINEL") {
                    curPath.hadSentinel();
                    ++it;
                    ++idx;
                    continue;
                }
                else {
                    curPath.hadOther();
                    ++it;
                    ++idx;
                    continue;
                }
            }
            continue;
        }
        else if (command == ":prev") {
            int amount = 0, count = 0;
            if (ss.good()) {
                ss >> amount;
                count = 1;
            }
            if ((idx - amount) < 1) {
                cout << "Prev gaat niet, je zou buiten de enquete terechtkomen!" << endl;
                continue;
            }
            idx = idx - amount;
            while (count <= amount) {
                groupQuestion * gq = dynamic_cast<groupQuestion *>(*it);
                groupSentinel * gs = dynamic_cast<groupSentinel *>(*it);
                if (gq != NULL) {
                    curPath.hadGroup(1);
                }
                if (gs != NULL) {
                    curPath.hadSentinel(1);
                }
                else {
                    curPath.hadOther(1);
                    count++;
                }
                --it;
                continue;
            }
            while ((it->questionType() == "GROUP" || it->questionType() == "SENTINEL" || it->answered == 1) && idx > 1) {
                if (it->questionType() == "GROUP") {
                    curPath.hadGroup(1);
                    --it;
                    //--idx;
                    continue;
                }
                else if (it->questionType() == "SENTINEL") {
                    curPath.hadSentinel(1);
                    //--idx;
                    --it;
                    continue;
                }
                else {
                    curPath.hadOther(1);
                    --idx;
                    --it;
                    continue;
                }
            }
                continue;
        }
        else {
            if (it->questionType() == "TEXT") {
                if (command.empty()) {
                    cerr << "Het antwoord mag niet leeg zijn!" << endl;
                    continue;
                }
                else {
                    it->setQuestionAnswer(command);
                    it->setAnswerend(1);
                }
            }
            if (it->questionType() == "BOOL") {
                if (command == "y" || command == "j" || command == "1") {
                    it->setQuestionAnswer("y");
                    it->setAnswerend(1);
                }
                else if (command == "n" || command == "0") {
                    it->setQuestionAnswer("n");
                    it->setAnswerend(1);
                }
                else {
                    cerr << "Ongeldig antwoord!" << endl;
                    continue;
                }
            }
            if (it->questionType() == "SCALE") {
                scaleQuestion * sq = dynamic_cast<scaleQuestion *>(*it);
                int low = sq->scale_l(), high = sq->scale_h(), answerInt;
                istringstream scaleStream(command);
                scaleStream >> answerInt;
                if (low <= answerInt && answerInt <= high) {
                    sq->setQuestionAnswer(command);
                    sq->setAnswerend(1);
                }
                else {
                    cerr << "Antwoord moet >= " << low << " zijn en <= " << high << endl;
                    continue;
                }
            }
            if (it->questionType() == "CHOICE") {
                choiceQuestion * cq = dynamic_cast<choiceQuestion *>(*it);
                int amountChoices = cq->amountChoices(), answerInt;
                istringstream choiceStream(command);
                choiceStream >> answerInt;
                if (1 <= answerInt && answerInt <= amountChoices) {
                    cq->setQuestionAnswer(command);
                    cq->setAnswerend(1);
                }
                else {
                    cerr << "Antwoord moet tussen 1 en " << amountChoices << " liggen!" << endl;
                    continue;
                }
            }
        }
        groupQuestion * gq = dynamic_cast<groupQuestion *>(*it);
        groupSentinel * gs = dynamic_cast<groupSentinel *>(*it);
        if (gq != NULL) {
            curPath.hadGroup();
            it++;
            //++idx;
            continue;
        }
        if (gs != NULL) {
            curPath.hadSentinel();
            it++;
            //++idx;
            continue;
        }
        else {
            curPath.hadOther();
            it++;
            ++idx;
            continue;
        }
    }
    enquete.saveAnswers(answerFile);
    return 0;
}

int editor_input(Enquete &enquete, const string& filename) {
	int done_with_running = 0;
	string invalidPath = "Opgeldig(e) pad(en)", invalidType = "Ongeldig type!";
	while (done_with_running == 0) {
		string input;
		getline(cin, input);
		istringstream u_input(input);
		u_input >> input;
		if (enquete.checkCaseInsensitive(input, "list")) {
			enquete.list();
		}
        if (enquete.checkCaseInsensitive(input, "askerlist")) {
            enquete.listAnswered();
        }
        if (enquete.checkCaseInsensitive(input, "test")) {
            asker(enquete, 1);
            cout << "Back in editor-mode" << endl;
        }
		if (enquete.checkCaseInsensitive(input, "add")) {
			string questionType;
			u_input >> questionType;
            int scaleL, scaleH;
            // Misschien is questionType wel een geldig path
            questionPath path;
            path.parseString(questionType);
            if (enquete.checkPath(path)) {
                u_input >> questionType;
                // Is het type v/d vraag wel geldig?
                if (enquete.checkQuestionType(questionType)) {
                    if (enquete.checkCaseInsensitive(questionType, "scale")) {
                        u_input >> scaleL >> scaleH;
                        string questionText;
                        u_input >> ws;
                        getline(u_input, questionText);
                        // Is de vraagtekst misschien leeg?
                        if (questionText.empty()) {
                            cerr << "De vraag mag niet leeg zijn. Add is geannuleerd!" << endl;
                            continue;
                        }
                        else if (scaleL >= scaleH) {
                            cerr << "L moet kleiner zijn dan H!" << endl;
                            continue;
                        }
                        else {
                            enquete.insert(questionType, questionText, path, scaleL, scaleH, 1);
                            continue;
                        }
                    }
                    else {
                        string questionText;
                        u_input >> ws;
                        getline(u_input, questionText);
                        // Is de vraagtekst misschien leeg?
                        if (questionText.empty()) {
                            cerr << "De vraag mag niet leeg zijn. Add is geannuleerd!" << endl;
                        }
                        else {
                            enquete.insert(questionType, questionText, path, 0, 0, 1);
                        }
                    }
                }
                else {
                    cerr << invalidType << endl;
                }
            }
			// Is het type v/d vraag wel geldig?
			else if (enquete.checkQuestionType(questionType)) {
                if (enquete.checkCaseInsensitive(questionType, "scale")) {
                    u_input >> scaleL >> scaleH;
                    string questionText;
                    u_input >> ws;
                    getline(u_input, questionText);
                    // Is de vraagtekst misschien leeg?
                    if (questionText.empty()) {
                        cerr << "De vraag mag niet leeg zijn. Add is geannuleerd!" << endl;
                        continue;
                    }
                    else if (scaleL >= scaleH) {
                        cerr << "L moet kleiner zijn dan H!" << endl;
                        continue;
                    }
                    else {
                        enquete.add(questionType, questionText, scaleL, scaleH);
                        cout << "Vraag (" << questionText << ") toegevoegd aan het einde van de enquete" << "." << endl;
                        continue;
                    }
                }
                else {
                    string questionText;
                    u_input >> ws;
                    getline(u_input, questionText);
                    // Is de vraagtekst misschien leeg?
                    if (questionText.empty()) {
                        cerr << "De vraag mag niet leeg zijn. Add is geannuleerd!" << endl;
                    }
                    else {
                        enquete.add(questionType, questionText);
                        cout << "Vraag (" << questionText << ") toegevoegd aan het einde van de enquete" << "." << endl;
                    }
                }
			}
			else {
				cerr << invalidType << endl;
			}
		}
		if (enquete.checkCaseInsensitive(input, "insert")) {
			questionPath path;
			string pathString, questionType;
            int scaleL, scaleH;
			u_input >> pathString >> questionType;
            path.parseString(pathString);
			// Is het type v/d vraag wel geldig?
			if (enquete.checkQuestionType(questionType)) {
				// Zit het getal wel in het interval?
				if (enquete.checkPath(path)) {
                    if (enquete.checkCaseInsensitive(questionType, "scale")) {
                        u_input >> scaleL >> scaleH;
                        string questionText;
                        u_input >> ws;
                        getline(u_input, questionText);
                        // Is de vraagtekst misschien leeg?
                        if (questionText.empty()) {
                            cerr << "De vraag mag niet leeg zijn. Insert is geannuleerd!" << endl;
                        }
                        else if (scaleL >= scaleH) {
                            cerr << "L moet kleiner zijn dan H!" << endl;
                            continue;
                        }
                        else {
                            enquete.insert(questionType, questionText, path, scaleL, scaleH);
                            cout << "Vraag (" << questionText << ") toegevoegd voor vraag ";
                            path.printPath();
                            cout << endl;
                        }
                    }
                    else {
                        string questionText;
                        u_input >> ws;
                        getline(u_input, questionText);
                        // Is de vraagtekst misschien leeg?
                        if (questionText.empty()) {
                            cerr << "De vraag mag niet leeg zijn. Insert is geannuleerd!" << endl;
                        }
                        else {
                            enquete.insert(questionType, questionText, path);
                            cout << "Vraag (" << questionText << ") toegevoegd voor vraag ";
                            path.printPath();
                            cout << endl;
                        }
                    }
				}
				else {
					cerr << invalidPath << endl;
				}
			}
			else {
				cerr << invalidType << endl;
			}
		}
		if (enquete.checkCaseInsensitive(input, "edit")) {
			string questionType;
			u_input >> questionType;
			// Is het type van de vraag 'choice'?
			if (enquete.checkCaseInsensitive(questionType, "choice")) {
				questionPath path;
                string pathString;
				u_input >> pathString;
                path.parseString(pathString);
				if (enquete.checkPath(path)) {
					enquete.setChoices(path);
				}
				else {
					cerr << invalidPath << endl;
				}
			}
			else {
				// Het is geen CHOICE, in type zit nu een string voor ons path
				// We moeten de string dus omzetten naar een bruikbaar path
				questionPath path;
				path.parseString(questionType);
				if (enquete.checkPath(path)) {
                    cout << "in else" << endl;
					enquete.edit(path);
				}
				else {
					cerr << invalidPath << endl;
				}
			}
		}
        if (enquete.checkCaseInsensitive(input, "group")) {
            string path1String, path2String;
            questionPath path1, path2;
            u_input >> path1String >> path2String;
            path1.parseString(path1String);
            path2.parseString(path2String);
            if (enquete.checkPath(path1) && enquete.checkPath(path2) && path1.sameLevel(path2) && path1.before(path2)) {
                string groupTheme;
                u_input >> ws;
                getline(u_input, groupTheme);
                if (groupTheme.empty()) {
                    cerr << "Thema van de group mag niet leeg zijn!" << endl;
                }
                else {
                    int amountSubgroups = path1.lengthPath(path2);
                    enquete.add_group(groupTheme, path1, path2, amountSubgroups);
                }
            }
            else {
                cerr << "Ongeldige paden opgegeven!" << endl;
            }
        }
        if (enquete.checkCaseInsensitive(input, "ungroup")) {
            string pathString;
            questionPath path;
            u_input >> pathString;
            path.parseString(pathString);
            if (enquete.checkPath(path) && enquete.checkGroupPath(path)) {
                enquete.ungroup(path);
            }
            else {
                cerr << invalidPath << endl;
            }
        }
		if (enquete.checkCaseInsensitive(input, "remove")) {
			questionPath path;
            string pathString;
            u_input >> pathString;
            path.parseString(pathString);
			if (enquete.checkPath(path)) {
                if (enquete.checkGroupPath(path)) {
                    cerr << "Gebruik ungroup i.p.v. remove om een group te verwijderen." << endl;
                }
                else {
                    enquete.remove(path);
                }
			}
			else {
				cerr << invalidPath << endl;
			}
		}
		if (enquete.checkCaseInsensitive(input, "save")) {
			enquete.save(filename);
		}
		if (enquete.checkCaseInsensitive(input, "quit")) {
			if (enquete.notSaved()) {
				string save_answer;
				cout << "Er zijn onbewaarde wijzigingen. Moeten deze opgeslagen worden? (j/n)" << endl;
				getline(cin, save_answer);
				if (enquete.checkCaseInsensitive(save_answer, "j")) {
					enquete.save(filename);
					done_with_running = 1;
				}
				else if (enquete.checkCaseInsensitive(save_answer, "n")) {
					done_with_running = 1;
				}
				else {
					cerr << "Ongeldig antwoord. Afsluiten afgebroken." << endl;
				}
			}
			else {
				done_with_running = 1;
			}
		}
	}
    return 0;
}

Enquete editor(const string& filename) {
	const unsigned int max_choices = 10;
	Enquete enquete(10, 5);
	ifstream file(filename);
    vector<int> groupVector;
    groupVector.resize(1);
    groupVector[0] = 0;
	if (file.is_open()) {
		// Bestand bestaat al, we moeten het dus inlezen en verwerken
		int headercount = 1, headerlength = 3, h_version_l = 1, headeridline = 2, headerquestionsline = 3;
		string a_string;
		while (getline(file, a_string)) {
			istringstream ssquestion(a_string);
            
			// We verwerken hier de header van de enquete
			if (headercount <= headerlength) {
				// Versie van de enquete inlezen
				if (headercount == h_version_l) {
					string version_text;
					int version_num;
					ssquestion >> version_text >> version_num;
					enquete.setVersion(2);
				}
				// ID van enquete inlezen
				if (headercount == headeridline) {
					string id_text, id_code;
					ssquestion >> id_text >> id_code;
					enquete.setID(id_code);
				}
				// Aantal vragen van de enquete inlezen
				if (headercount == headerquestionsline) {
					string steps_text;
					int steps_num;
					ssquestion >> steps_text >> steps_num;
				}
				headercount++;
				continue;
			}
            
			// We lopen door de eerste twee "woorden" in de stringstream en slaan deze op in hun variabelen
			string questionNum, questionType;
			ssquestion >> questionNum >> questionType;
            
			if (enquete.checkCaseInsensitive(questionType, "choice")) {
				unsigned int num_of_choices;
				ssquestion >> num_of_choices;
				if (num_of_choices <= max_choices) {
					string questionText;
                    vector<string> choices;
                    choices.resize(1);
                    ssquestion >> ws;
					getline(ssquestion, questionText);
					// Keuzes verwerken
					unsigned int c_a_idx = 1, choicecount = 1;
					while (choicecount <= num_of_choices) {
						string choice;
						(getline(file, choice));
						choices.push_back(choice);
						choicecount++;
						c_a_idx++;
					}
					enquete.add(questionText, choices, num_of_choices);
                    groupVector.push_back(0);
                    continue;
				}
			}
            if ((enquete.checkCaseInsensitive(questionType, "text")) || (enquete.checkCaseInsensitive(questionType, "bool"))) {
				string questionText;
                ssquestion >> ws;
				getline(ssquestion, questionText); // 'Echte' vraag vastpakken
				enquete.add(questionType, questionText);
                groupVector.push_back(0);
                continue;
			}
            if (enquete.checkCaseInsensitive(questionType, "scale")) {
                int l, h;
                string questionText;
                ssquestion >> l >> h;
                ssquestion >> ws;
                getline(ssquestion, questionText);
                enquete.add(questionType, questionText, l, h);
                groupVector.push_back(0);
                continue;
            }
            if (enquete.checkCaseInsensitive(questionType, "group")) {
                int amountSubgroups;
                string groupTheme;
                ssquestion >> amountSubgroups;
                ssquestion >> ws;
                getline(ssquestion, groupTheme);
                enquete.add_group(groupTheme, amountSubgroups);
                groupVector.push_back(amountSubgroups);
                groupVector[0]++;
                continue;
            }
		}
        enquete.prepareGroupVector(groupVector);
        enquete.useGroupVector(groupVector);
	}
	else {
		// Bestand bestaat nog niet, UUID aanmaken en wegschrijven
        cout << "Doesn't exist" << endl;
		string uuid = get_uuid();
		enquete.setID(uuid);
		enquete.setVersion(2);
	}
	file.close();
    enquete.setChanged(0);
    return enquete;
}

int main(int argc, const char *argv[]) {
	int numberargs;
	string file;
	numberargs = argc - 1;
    // 1 argument -> oproepen in editor
	if (numberargs == 1) {
		file = argv[1];
		Enquete enq = editor(file);
        editor_input(enq, file);
	}
    // 2 argumenten -> oproepen in asker
    else if (numberargs == 2) {
        string inputFile = argv[1], outputFile = argv[2];
        Enquete enq = editor(inputFile);
        asker(enq, 0, outputFile);
    }
	else {
		cerr << "Ongeldig aantal parameters!" << endl;
	}
	return 0;
}


