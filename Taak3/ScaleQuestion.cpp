/*
 * ScaleQuestion.cpp
 *
 *  Created on: 1 dec. 2013
 *      Author: Jannick
 */

#include "ScaleQuestion.h"
#include "Question.h"

scaleQuestion::scaleQuestion(unsigned int vectorSize):
Question(vectorSize), scale_l_(0), scale_h_(0) {}

void scaleQuestion::parseQuestion(const string& vraag, const int& scale_l, const int& scale_h) {
	setQuestionType("SCALE");
	setQuestionText(vraag);
	scale_l_ = scale_l;
	scale_h_ = scale_h;
}

const unsigned int& scaleQuestion::scale_l() const {
	return scale_l_;
}

const unsigned int& scaleQuestion::scale_h() const {
	return scale_h_;
}

void scaleQuestion::update_scale_l(const unsigned int& new_scale_l) {
	scale_l_ = new_scale_l;
}

void scaleQuestion::update_scale_h(const unsigned int& new_scale_h) {
	scale_h_ = new_scale_h;
}
