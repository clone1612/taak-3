/*
 * ChoiceQuestion.cpp
 *
 *  Created on: 1 dec. 2013
 *      Author: Jannick
 */

#include "ChoiceQuestion.h"
#include "Question.h"

choiceQuestion::choiceQuestion(const unsigned int& vectorSize): amountChoices_(0), Question(vectorSize) {
    choices_.resize(1);
}

void choiceQuestion::parseQuestion(const string& vraag, vector<string> choices, const int& amountChoices) {
	setQuestionType("CHOICE");
	setQuestionText(vraag);
	choices_ = choices;
    amountChoices_ = amountChoices;
}

const int& choiceQuestion::amountChoices() {
	return amountChoices_;
}

void choiceQuestion::printChoices() const {
    int idx = 1, num = 1;
	while (idx <= amountChoices_) {
		cout << num << ") " << choices_[idx] << endl;
		idx++;
        num++;
	}
}

const string& choiceQuestion::choiceAt(const int &idx) const {
    return choices_[idx];
}

void choiceQuestion::setAmountChoices(const unsigned int& newAmountChoices) {
	amountChoices_ = newAmountChoices;
}

void choiceQuestion::setChoices(const vector<string> newChoices) {
    choices_ = newChoices;
}