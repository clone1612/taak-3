/*
 * GroupSentinel.cpp
 *
 *  Created on: 6 dec. 2013
 *      Author: Jannick
 */

#include "GroupSentinel.h"

groupSentinel::groupSentinel(const int& groupID): Question(5), groupID_(groupID) {
	setQuestionType("SENTINEL");
}

const int& groupSentinel::groupID() const {
	return groupID_;
}
