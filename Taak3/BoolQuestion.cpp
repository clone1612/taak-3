/*
 * BoolQuestion.cpp
 *
 *  Created on: 1 dec. 2013
 *      Author: Jannick
 */

#include "BoolQuestion.h"
#include "Question.h"

boolQuestion::boolQuestion(const unsigned int& vectorSize):
Question(vectorSize) {}

void boolQuestion::parseQuestion(const string& vraag) {
	setQuestionType("BOOL");
	setQuestionText(vraag);
}
