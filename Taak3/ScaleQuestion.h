/*
 * ScaleQuestion.h
 *
 *  Created on: 1 dec. 2013
 *      Author: Jannick
 */

#ifndef SCALEQUESTION_H_
#define SCALEQUESTION_H_

#include "Question.h"

class scaleQuestion: public Question {
public:
	scaleQuestion(unsigned int vectorSize);

	void parseQuestion(const string& vraag, const int& scale_l, const int& scale_h);

	const unsigned int& scale_l() const;
	const unsigned int& scale_h() const;
	void update_scale_l(const unsigned int& new_scale_l);
	void update_scale_h(const unsigned int& new_scale_h);
private:
	unsigned int scale_l_, scale_h_;
};

#endif /* SCALEQUESTION_H_ */
