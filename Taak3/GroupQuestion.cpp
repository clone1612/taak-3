/*
 * GroupQuestion.cpp
 *
 *  Created on: 1 dec. 2013
 *      Author: Jannick
 */

#include "GroupQuestion.h"
#include "Question.h"
#include "GroupSentinel.h"

groupQuestion::groupQuestion(const unsigned int& vectorSize, const unsigned int& groupID):
Question(vectorSize), groupID_(groupID), sentinel_(NULL) {}

void groupQuestion::parseQuestion(const string& theme) {
	setQuestionType("GROUP");
	setQuestionText(theme);
}

const int& groupQuestion::groupID() const {
	return groupID_;
}

const int& groupQuestion::amountSubgroups() const {
    return amountSubgroups_;
}

void groupQuestion::setAmountSubgroups(const int &newAmount) {
    amountSubgroups_ = newAmount;
}

groupSentinel* groupQuestion::sentinel() {
    return sentinel_;
}

void groupQuestion::setSentinel(groupSentinel *newSentinel) {
    sentinel_ = newSentinel;
}