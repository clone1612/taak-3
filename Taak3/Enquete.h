/*
 * Enquete.h
 *
 *  Created on: 25 nov. 2013
 *      Author: Jannick
 */

#include <iostream>
#include <vector>
#include <stack>
#include "Question.h"
#include "GroupQuestion.h"
#include "questionPath.h"
using namespace std;

#ifndef ENQUETE_H_
#define ENQUETE_H_

class Enquete {
public:
	Enquete(unsigned int max_choices, unsigned int max_depth): root_(new Question(5)), last_(root_), max_choices_(max_choices),
    max_depth_(max_depth), groupID_(1) {}
	// Print alle vragen in onze lijst
	void list();
    void listAnswered();

	// Controleer of het type vraag geldig is
	static bool checkQuestionType(const string& q_type);
    bool checkPath(const questionPath& path);
    bool checkGroupPath(const questionPath &path);
    void updatePath(Question* curQuestion, questionPath &curPath);
    void updatePath(stack<groupQuestion*> &groupStack, Question* curQuestion, questionPath &curPath);

	// Controleer of twee strings gelijk zijn (niet-case-sensitive)
	static bool checkCaseInsensitive(const string& a, const string& b); // 2 strings controleer -> gelijk case-insensitive

	void setID(const string& newID); // Pas de UUID v/d enquete aan

	void setVersion(const unsigned int& newVersion); // Pas de versie v/d enquete aan

	// Voeg een nieuwe vraag toe aan onze lijst
	void add(const string& soort, const string& vraag, const unsigned int& scale_l = 0, const unsigned int& scale_h = 0);
	void add(const string& vraag, vector<string> choices, const int& n_choices);
    void insert(const string& soort, const string& vraag, const questionPath& path, const int& scale_l = 0, const int& scale_h = 0, const int& beforeSentinel = 0);

	void add_group(const string& name, const int& amountSubgroups); // Voeg groep toe op einde v/d enquete
    void prepareGroupVector(vector<int>& groupVector);
    void useGroupVector(vector<int>& groupVector);
	void add_group(const string& name, questionPath& path1, questionPath& path2, const int& amountSubgroups);
	void ungroup(questionPath& path);

	const int amountQuestions(); // Geeft terug hoeveel vragen er in een lijst zijn

	void edit(const questionPath& path); // Pas de vraagtekst van een specifieke vraag aan
    void setChoices(const questionPath& path); // Pas de keuzes voor een specifieke choice-vraag aan

	void remove(const questionPath& path); // Verwijdert een specifieke vraag

	void setChanged(const int& changedStatus); // We hebben iets gewijzigd, of het bestand opgeslagen

	bool notSaved() const; // Geeft aan of er nog niet-opgeslagen wijzigingen zijn

	void save(const string& filename); // Slaat de lijst op
    void saveAnswers(const string &filename);
    
    typedef const Question& const_reference;
    typedef const Question* const_pointer;
    typedef Question* pointer;
    
    class Iterator: public iterator<bidirectional_iterator_tag, Question> {
    public:
        Iterator(): questionPointer_(NULL) {}
        Iterator(Question* q) : questionPointer_(q) {}
        ~Iterator() {}
        
        Iterator& operator=(const Iterator& other) {
            questionPointer_ = other.questionPointer_;
            return *this;
        }
        
        bool operator==(const Iterator& other) {
            return (questionPointer_ == other.questionPointer_);
        }
        
        bool operator!=(const Iterator& other) {
            return (questionPointer_ != other.questionPointer_);
        }
        
        Iterator& operator++() {
            if (questionPointer_ != NULL) {
                questionPointer_ = questionPointer_->next_;
            }
            return *this;
        }
        
        Iterator& operator--() {
            if (questionPointer_ != NULL) {
                questionPointer_ = questionPointer_->prev_;
            }
            return *this;
        }
        
        Iterator operator++(int) {
            Iterator tmp(*this);
            ++(*this);
            return tmp;
        }
        
        Iterator operator--(int) {
            Iterator tmp(*this);
            --(*this);
            return tmp;
        }
        
        Question* operator*()
        {
            return questionPointer_;
        }
        
        pointer& operator->()
        {
            return(questionPointer_);
        }
        
    private:
        Question* questionPointer_;
    };
    
    Iterator insert_after(Question* qBefore, Question* qNew);
    Iterator insert_before(Question* qAfter, Question* qNew);
    void push_back(Question* qNew);
    void pop_back();
    bool checkGroupAnswered(Iterator it);
    Iterator erase(Question* q);
    Iterator begin() {
        return Iterator(root_->next_);
    }
    Iterator end() {
        return Iterator();
    }
private:
	Question* root_;
    Question* last_;
	string id_;
	unsigned int version_ = 1, changed_ = 0, max_choices_, max_depth_, groupID_;
};

#endif /* ENQUETE_H_ */
